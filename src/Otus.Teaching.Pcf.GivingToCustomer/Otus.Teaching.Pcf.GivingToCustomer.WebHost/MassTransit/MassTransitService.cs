﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using Otus.Teaching.Pcf.MassTransitCore;
using Otus.Teaching.Pcf.MassTransitCore.Contracts;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.MassTransit
{
    public class MassTransitService : IMassTransitService<PromocodeCreatedContract>
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;

        public MassTransitService(IRepository<PromoCode> promoCodesRepository, IRepository<Preference> preferencesRepository, IRepository<Customer> customersRepository)
        {
            _promoCodesRepository = promoCodesRepository;
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
        }

        public async Task<bool> ExecuteAsync(PromocodeCreatedContract contract)
        {
            if (contract == null || contract.PreferenceId == null)
            {
                return false;
            }

            //Получаем предпочтение по имени
            var preference = await _preferencesRepository.GetByIdAsync(contract.PreferenceId);

            if (preference == null)
            {
                return false;
            }

            //  Получаем клиентов с этим предпочтением:
            var customers = await _customersRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));

            var request = new GivePromoCodeRequest()
            {
                ServiceInfo = contract.ServiceInfo,
                PartnerId = contract.PartnerId,
                PromoCodeId = contract.PromoCodeId,
                PromoCode = contract.PromoCode,
                PreferenceId = contract.PreferenceId,
                BeginDate = contract.BeginDate,
                EndDate = contract.EndDate
            };

            PromoCode promoCode = PromoCodeMapper.MapFromModel(request, preference, customers);

            await _promoCodesRepository.AddAsync(promoCode);

            return true;
        }
    }
}
