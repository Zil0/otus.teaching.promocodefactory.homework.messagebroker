﻿using GreenPipes;
using MassTransit;
using MassTransit.ConsumeConfigurators;
using MassTransit.Definition;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.MassTransit.PromocodeCreated
{
    public class PromocodeCreatedConsumerDefinition : ConsumerDefinition<PromocodeCreatedConsumer>
    {
        protected override void ConfigureConsumer(IReceiveEndpointConfigurator endpointConfigurator, IConsumerConfigurator<GivingToCustomer.WebHost.MassTransit.PromocodeCreated.PromocodeCreatedConsumer> consumerConfigurator)
        {
            endpointConfigurator.UseRetry(x => x.Intervals(100, 200, 500, 750));
        }
    }
}
