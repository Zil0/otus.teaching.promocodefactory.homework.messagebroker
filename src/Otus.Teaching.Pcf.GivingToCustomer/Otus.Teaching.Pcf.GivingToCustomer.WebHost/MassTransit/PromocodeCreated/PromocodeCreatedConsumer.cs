﻿using System.Threading.Tasks;
using MassTransit;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using Otus.Teaching.Pcf.MassTransitCore;
using Otus.Teaching.Pcf.MassTransitCore.Contracts;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.MassTransit.PromocodeCreated
{
    public class PromocodeCreatedConsumer : IConsumer<PromocodeCreatedContract>
    {
        private IMassTransitService<PromocodeCreatedContract> _service;

        public PromocodeCreatedConsumer(IMassTransitService<PromocodeCreatedContract> service)
        {
            _service = service;
        }

        public async Task Consume(ConsumeContext<PromocodeCreatedContract> context)
        {
            PromocodeCreatedContract contract = new PromocodeCreatedContract
            {
                ServiceInfo = context.Message.ServiceInfo,
                PartnerId = context.Message.PartnerId,
                PromoCodeId = context.Message.PromoCodeId,
                PromoCode = context.Message.PromoCode,
                PreferenceId = context.Message.PreferenceId,
                BeginDate = context.Message.BeginDate,
                EndDate = context.Message.EndDate
            };

            await _service.ExecuteAsync(contract);
        }
    }
}
