﻿
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.MassTransitCore
{
    public interface IMassTransitService<T> where T : class
    {
        public Task<bool> ExecuteAsync(T contract);
    }
}
