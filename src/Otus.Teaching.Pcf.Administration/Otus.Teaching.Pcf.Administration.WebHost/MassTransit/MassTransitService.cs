﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.MassTransitCore;
using Otus.Teaching.Pcf.MassTransitCore.Contracts;

namespace Otus.Teaching.Pcf.Administration.WebHost.MassTransit
{
    public class MassTransitService : IMassTransitService<UpdateAppliedPromocodesContract>
    {
        private readonly IRepository<Employee> _employeeRepository;

        public MassTransitService(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public async Task<bool> ExecuteAsync(UpdateAppliedPromocodesContract contract)
        {
            var employee = await _employeeRepository.GetByIdAsync(contract.Id);

            if (employee == null)
                return false;

            employee.AppliedPromocodesCount++;

            await _employeeRepository.UpdateAsync(employee);
            return true;
        }
    }
}
