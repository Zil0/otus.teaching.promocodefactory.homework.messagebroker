﻿using System.Threading.Tasks;
using MassTransit;

using Otus.Teaching.Pcf.MassTransitCore;
using Otus.Teaching.Pcf.MassTransitCore.Contracts;

namespace Otus.Teaching.Pcf.Administration.WebHost.MassTransit.UpdateAppliedPromocodes
{
    public class UpdateAppliedPromocodesConsumer : IConsumer<UpdateAppliedPromocodesContract>
    {
        private IMassTransitService<UpdateAppliedPromocodesContract> _service;

        public UpdateAppliedPromocodesConsumer(IMassTransitService<UpdateAppliedPromocodesContract> service)
        {
            _service = service;
        }

        public async Task Consume(ConsumeContext<UpdateAppliedPromocodesContract> context)
        {
            var contract = new UpdateAppliedPromocodesContract()
            {
                Id = context.Message.Id
            };
            await _service.ExecuteAsync(contract);
        }
    }
}
