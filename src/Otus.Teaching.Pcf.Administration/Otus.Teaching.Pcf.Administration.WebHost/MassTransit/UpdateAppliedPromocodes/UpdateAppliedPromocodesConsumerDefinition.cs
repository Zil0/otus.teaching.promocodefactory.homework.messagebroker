﻿using GreenPipes;
using MassTransit;
using MassTransit.ConsumeConfigurators;
using MassTransit.Definition;

namespace Otus.Teaching.Pcf.Administration.WebHost.MassTransit.UpdateAppliedPromocodes
{
    public class UpdateAppliedPromocodesConsumerDefinition : ConsumerDefinition<UpdateAppliedPromocodesConsumer>
    {
        protected override void ConfigureConsumer(IReceiveEndpointConfigurator endpointConfigurator, IConsumerConfigurator<UpdateAppliedPromocodesConsumer> consumerConfigurator)
        {
            endpointConfigurator.UseRetry(x => x.Intervals(100, 200, 500, 750));
        }
    }
}
